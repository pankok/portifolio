import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { NgbDateStruct, NgbCarouselConfig, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styles: [`
    /* ngb-progressbar {
        margin-top: 5rem;
    } */
   
    `],
})

export class ComponentsComponent implements OnInit {
    images = [1, 2, 3].map(() => `https://picsum.photos/900/500?random&t=${Math.random()}`);
    showNavigationArrows = true;
    showNavigationIndicators = true;
    pauseOnHover = true;

    @ViewChild('mycarousel', { static: true }) carousel: NgbCarousel;

    constructor() { }

    ngOnInit() {
    }



    startCarousel() {
        this.carousel.cycle();
    }

    pauseCarousel() {
        this.carousel.pause();
    }

    moveNext() {
        this.carousel.next();
    }

    getPrev() {
        this.carousel.prev();
    }

    goToSlide(slide) {
        this.carousel.select(slide);
    }

}